package com.example.mapwithmodelclass;

public class ModelClass {
    private int image;
    private String describe;
    private String titele;
    private Double lat;
    private Double lng;

    public ModelClass(int image, String describe, String titele, Double lat, Double lng) {
        this.image = image;
        this.describe = describe;
        this.titele = titele;
        this.lat = lat;
        this.lng = lng;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getTitele() {
        return titele;
    }

    public void setTitele(String titele) {
        this.titele = titele;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

}
