package com.example.mapwithmodelclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.Display;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
private List<ModelClass>  mymodellist= new ArrayList<>();
GoogleMap mymap;
    static LatLng location = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapfrg=(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.myfg1);
        mapfrg.getMapAsync(this);

     mymodellist.add(new ModelClass(R.drawable.ic_directions,"this City  ","TamWorth",-31.0983332,150.916672));
     mymodellist.add(new ModelClass(R.drawable.ic_directions,"This City  ","NewCastle",-32.916668,151.750000));
        mymodellist.add(new ModelClass(R.drawable.ic_directions,"This City  ","Brisbane",-27.470125,153.021072));
        mymodellist.add(new ModelClass(R.drawable.ic_directions,"This City  ","Dubbi",-32.256943,148.601105));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mymap=googleMap;
        mymap.getUiSettings().setZoomControlsEnabled(true);
        mymap.getUiSettings().setAllGesturesEnabled(true);
        for (int i=0;i<mymodellist.size();i++){
            location=new LatLng(mymodellist.get(i).getLat(),mymodellist.get(i).getLng());
            mymap.addMarker(new MarkerOptions()
                    .position(location)
                    .title(mymodellist.get(i).getTitele())
                    .snippet(mymodellist.get(i).getDescribe())

                    .draggable(true)
                   .icon(BitmapDescriptorFactory.fromResource(R.drawable.myicon)));
        }
        mymap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14));


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
